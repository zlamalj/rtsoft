<?php

namespace App\Models;

class PriceEUcalculator
{
    public function calculatePrice($priceCZK, $code){
        //https://stackoverflow.com/questions/2019892/extract-data-from-website-via-php
        $links = explode("\n",file_get_contents('https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt?date=22.04.2022'));

        //Create a new array to save
        $price = array();

        /*
         * Seperate "|"
         */
        foreach ($links as $link){
            $price [] = explode("|", $link);
        }

        /*
         * Remove zero index
         */
        unset($price[0]);

        /*
         * Searching right price with code
         */
        foreach ($price as $value){
            if($code == $value[3]){
                return round($priceCZK / floatval(str_replace(",",".",$value[4])),"2");
            }
        }

    }
}