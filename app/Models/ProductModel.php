<?php
namespace App\Models;
use Nette\Application\UI\Form;

class ProductModel extends BaseModel {

    public function __construct(\Nette\Database\Explorer $database)
    {
        parent::__construct($database);
    }
    public function getMaxProductId(){
        $result = $this->database->table("products")->max("id");

        return $result + 1;
    }
    public function getProduct($id){
        $product = $this->database->table('products')->where("id=?",$id);

        $result = $product->fetch();

        return $result;
    }
    public function getCategoryAll(){
        return $this->database->table("category");
    }
    public function getCategory($id){
        $category  = $this->database->table("category")->where("id=?",$id);
        $result = $category->fetch();

        return $result;
    }
    public function getTag($id){
        $products = $this->database->table("products")->where("id=?",$id);
        foreach ($products as $product){
            $result = [];
            foreach ($product->related('product_tag') as $product_tag){
                $result[] = $product_tag->tag_id;
            }
            return $result;
        }

    }
    public function getTagAll(){
        return $this->database->table("tags");
    }
    public function getCategoryArray(){
        $category_array = [];
        $result = $this->getCategoryAll();
        foreach($result as $row){
            $category_array[$row->id] = $row->name;
        }
        return $category_array;
    }
    public function getTagArray(){
        $tag_array = [];
        $result = $this->getTagAll();
        foreach($result as $row){
            $tag_array[$row->id] = $row->name;
        }
        return $tag_array;
    }

    public function getDatabaseCount($table){
        return $this->database->table($table)->count();
    }


    public function createProduct($name, $priceCZK, $priceEU, $date, $category, $active){
        $this->database->table("products")->insert([
            'id' => $this->getMaxProductId(),
            'name' => $name,
            'priceCZK' => $priceCZK,
            'priceEU' => $priceEU,
            'date' => $date,
            'category' => $category,
            'active' => $active
        ]);
    }
    public function createTags($id ,$tags){
        foreach ($tags as $tag){
            $this->database->table("product_tag")->insert([
                'product_id' => $id,
                'tag_id' => $tag
            ]);
        }

    }
    public function editProduct($id, $name, $priceCZK, $priceEU, $date, $category, $active){
        $this->database->query('UPDATE products SET', [
            'name' => $name,
            'priceCZK' => $priceCZK,
            'priceEU' => $priceEU,
            'date' => $date,
            'category' => $category,
            'active' => $active
        ], 'WHERE id = ?', $id);
    }
    public function editTags($id, $tags){
        $this->database->table("product_tag")->where("product_id=?",$id)->delete();
        foreach ($tags as $tag){
            $this->database->table("product_tag")->insert([
                'product_id' => $id,
                'tag_id' => $tag
            ]);
        }
    }
    public function deleteProduct($id){
        $this->database->table("products")->where("id=?",$id)->delete();
    }
    public function deleteTags($id){
        $this->database->table("product_tag")->where("product_id=?",$id)->delete();
    }
 }

?>
