<?php
namespace App\Models;
use Nette\Database\Explorer;
use Nette;

 class BaseModel
 {
     public Nette\Database\Explorer $database;

     public function __construct(Nette\Database\Explorer $database)
     {
         $this->database = $database;
     }
 }
?>