<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Models\PriceEUcalculator;
use Nette;
use App\Models\ProductModel;
use Nette\Application\UI\Form;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    private ProductModel $ProductModel;
    private PriceEUcalculator $priceEUcalculator;

    public function __construct(ProductModel $ProductModel, PriceEUcalculator $priceEUcalculator)
    {
        $this->ProductModel = $ProductModel;
        $this->priceEUcalculator = $priceEUcalculator;
    }
    /*
     * CREATE product
     */
    protected function createComponentCreateProduct(): Form
    {

        /*
        * Category array
        */
        $category_array = $this->ProductModel->getCategoryArray();

        /*
         * Tag array
         */
        $tag_array = $this->ProductModel->getTagArray();

        $active = [
            '1' => 'Aktivní',
            '0' => 'Neaktivní'
        ];

        $form = new Form();
        $form->addHidden("max_id","id")
            ->setValue($this->ProductModel->getMaxProductId());
        $form->addText("name","Jméno")
            ->addRule(FORM::PATTERN,"Název může obsahovat pouze písmena a čísla","^[a-zA-Z0-9]*$")
            ->setRequired("Nevyplnili jste jméno");
        $form->addInteger("priceCZ","Cena v CZK")
            ->setRequired("Nevyplnili jste cenu");
        $form->addHidden("priceEU","Cena v EURO (počítání pomocí URL)");
        $form->addText("date","Datum")
            ->setRequired("Nevyplnili jste datum")
            ->setType("date");
        $form->addSelect("category","Kategorie:", $category_array)
            ->setRequired("Nevyplnili jste kategorii");
        $form->addMultiSelect("tag","Štítky:",$tag_array)
            ->setRequired("Nevyplnili jste šťítek");
        $form->addSelect("active","Dostupnost:",$active)
            ->setRequired("Nevyplnili jste dostupnost služby");
        $form->addSubmit('send', 'Vytvořit produkt');
        $form->onSuccess[] = [$this, 'formSucceededCreateProduct'];
        return $form;
    }
    /*
     * EDIT product
     */

    protected function createComponentEditProduct(): Form
    {
        /*
         * ID
         */
        $id = $this->getHttpRequest()->getQuery("productId");

        /*
        * Product array
        */
        $product_array = $this->ProductModel->getProduct($id);

        $form = new Form();

        if($id !== null && $product_array !== null){
            /*
             * Category array
             */
            $category_array = $this->ProductModel->getCategoryArray();
            $category = $this->ProductModel->getCategory($product_array["category"]);
            /*
             * Tags array
             */
            $tag_array = $this->ProductModel->getTagArray();
            $tag = $this->ProductModel->getTag($product_array["id"]);
            if($tag == null){
                $tag["tag_id"] = null;
            }

            $active = [
                '1' => 'Aktivní',
                '0' => 'Neaktivní'
            ];
            $form->addText("idView","ID")
                ->setDisabled()
                ->setValue($product_array["id"])
            ;
            $form->addHidden("id","ID")
                ->setValue($product_array["id"])
            ;
            $form->addText("name","Jméno")
                ->setRequired()
                ->addRule(FORM::PATTERN,"Název může obsahovat pouze písmena, čísla a mezery","^[a-zA-Z0-9]*$")
                ->setValue($product_array["name"]);
            $form->addInteger("priceCZ","Cena v CZK")
                ->setRequired()
                ->setValue($product_array["priceCZK"])
            ;
            $form->addHidden("priceEU","Cena v EURO (počítání pomocí URL)");
            $form->addText("date","Datum")
                ->setRequired()
                ->setType("date")
                ->setValue(date_format($product_array["date"],"Y-d-m"));
            $form->addSelect("category","Kategorie:", $category_array)
                ->setDefaultValue($category["id"])
                ->setRequired();

            $form->addMultiSelect("tag","Štítky:",$tag_array)
                ->setRequired()
                ->setValue($tag);
            $form->addSelect("active","Dostupnost:",$active)
                ->setRequired()
                ->setValue($product_array["active"])
            ;
            $form->addSubmit('send', 'Editovat');
            $form->onSuccess[] = [$this, 'formSucceededEditProduct'];

        }
        else{
            $this->flashMessage("Špatné ID produktu","error");
            $this->redirect("Homepage:listProducts");
        }
        return $form;
    }
    public function formSucceededCreateProduct(Form $form, $data){
        $this->ProductModel->createProduct($data->name, $data->priceCZ, $this->priceEUcalculator->calculatePrice($data->priceCZ,"EUR"),$data->date, $data->category, $data->active);
        $this->ProductModel->createTags($data->max_id,$data->tag);
        $this->flashMessage("Produkt byl úspěšně vytvořen");
        $this->redirect("Homepage:listProducts");
    }
    public function formSucceededEditProduct(Form $form, $data){
        $this->ProductModel->editProduct($data->id, $data->name, $data->priceCZ, $this->priceEUcalculator->calculatePrice($data->priceCZ,"EUR"),$data->date, $data->category, $data->active);
        $this->ProductModel->editTags($data->id,$data->tag);
        $this->flashMessage("Produkt byl úspěšně upraven");


    }
    public function renderListProducts(): void
    {
        $this->template->products = $this->ProductModel->database
            ->table('products')
            ->order('id ASC')
        ;
        $producCount = $this->ProductModel->getDatabaseCount("products");
        $this->template->productCount = $producCount;

        if($producCount == 0){
            $this->flashMessage("Neexistují žádné produkty","error");
        }
    }
    public function renderEditProduct(int $productId):void
    {
        $this->createComponentEditProduct();
    }
    public function renderDeleteProduct(int $productId):void
    {
        $this->ProductModel->deleteTags($productId);
        $this->ProductModel->deleteProduct($productId);
        $this->flashMessage("Produkt byl právě smazán","warning");
        $this->redirect("Homepage:listProducts");
    }
    public function renderDefault():void{
        $this->template->productCount = $this->ProductModel->getDatabaseCount("products");
        $this->template->tagsCount = $this->ProductModel->getDatabaseCount("tags");
        $this->template->categoryCount = $this->ProductModel->getDatabaseCount("category");
    }
}
